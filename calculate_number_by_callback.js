//โปรแกรมคำนวณชุดเลข บวก ลบ 

const numbers = [1, 2, 3, 4, 5];
const numberToCalculate = 3;
/*Start coding here 🤩*/

function multiplyBy(number, multiplier) {
    let result = numbers.map(i => i*multiplier);
    return result;
}

function subtractFrom(number, subtractor) {
    let result = numbers.map(i => i-subtractor);
    return result;
}
        
function processArray(arr, num, callbackOperation){
  let newArr = [];
  for (let i of callbackOperation(arr,num)) {
    newArr.push(i); 
  }
  return newArr;
}
                            
let multiplierArr = processArray(numbers, numberToCalculate, multiplyBy)
console.log(multiplierArr);
let subtractorArr = processArray(numbers, numberToCalculate, subtractFrom)
console.log(subtractorArr);
